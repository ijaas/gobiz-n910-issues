# 17 December 2020

## Issue

Sale request from android device returns Error Code 96.

---

[UL Tools Log Diff Viewer](https://www.diffchecker.com/iVcy3NVi)

**Linux**

Sale Date/Time: 17/12/2020 11:38 am

- [Sign-On](linux/signon.log.txt)
- [Sale Test 01a UL Tools Log](linux/sale-test-01a.ULTools.log.txt)
- [Sale Test 01a Raw Log](linux/sale-test-01a.log.txt)

**Android**

Sale Date/Time: 17/12/2020 12:00 pm

- [Sign-On](android/signon.log.txt)
- [Sale Test 01a UL Tools Log](android/sale-test-01a.ULTools.log.txt)
- [Sale Test 01a Raw Log](android/sale-test-01a.log.txt)
